<?php

namespace App\Http\Controllers;

use App\Unit;
use App\User;
use App\Images;
use App\Project;
use App\UnitAgent;
use App\ProjectAgent;
use App\ProjectImage;
use App\AssignProject;
use App\ProjectTemplate;
use Illuminate\Http\Request;
use App\AdditionalProjectDetail;
use App\Events\DeploymentNoticeEvent;
use Illuminate\Support\Facades\Storage;


class ProjectController extends Controller
{
    public $table;

    public function __construct()
    {
        $this->table = with(new Project)->getTable();
    }

     /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function projectLists(Request $request)
    {
        try
        {
            //get loggedin user
            $user = getLoggedInUser();

            //check validation
            $validator = app('validator')->make($request->all(),[
                'sortOrder'      => 'in:asc,desc',
                'pageNumber'     => 'numeric|min:1',
                'recordsPerPage' => 'numeric|min:0',
                'search'         => 'json',
                'isEnable'       => 'in:0,1'
            ],[]);

            if ($validator->fails()) {
                return createResponse(config('httpResponse.UNPROCESSED'),
                    "Request parameter missing.",
                    ['error' => $validator->errors()->first()]);
            }

            //sorting
            $sortBy = ($request->has('sortBy')) ? $request->get('sortBy') : config('pager.sortBy');
            $sortOrder = ($request->has('sortOrder')) ? $request->get('sortOrder') : config('pager.sortOrder');
            $pager = [];

             // Check if all records are requested
            if ($request->has('records') && $request->get('records') == 'all') {

                $projects = new Project;
                // To get list of projects assigned to Master Agents,Agents or Affiliates
                // if($user['role'] != 'Admin')
                //     $projects = $projects->select($this->table.'.*')->rightJoin('projectAgents','project.id','=','projectAgents.projectId')->where('projectAgents.userId',$user['id'])->where('project.deployedAt','!=',NULL);
                // else
                //     //To get list of projects created by Project Manager
                //    $projects = $projects->where('createdBy',$user['id']);

                if($user['role'] == 'Admin' || $user['role'] == 'User Manager')
                {
                    $projects = $projects->select('id', 'name');
                } else if($user['role'] == 'Project Manager') {
                    $projects = $projects->select($this->table.'.id', $this->table.'.name')->rightJoin('assignProjects','project.id','=','assignProjects.projectId')->where('assignProjects.userId',$user['id']);
                }

                if ($request->has('search')) {
                    // Decode json in to php array
                    $search = json_decode($request->get('search'),true);
                    // Get only required params
                    $search = array_filter($search, function($k){
                        return $k == 'id' || $k == 'firstName' || $k == 'lastName' || $k == 'companyName'  || $k == 'office' || $k == 'address' || $k == 'isEnable' || $k == 'role';
                    }, ARRAY_FILTER_USE_KEY);

                    foreach ($search as $field => $value)
                       $projects = $projects->where($this->table.'.'.$field, 'like', '%'.$value.'%');
                }
                $projects = $projects = $projects->orderBy($this->table.'.'.$sortBy, $sortOrder)->get();

            } else { // Else return paginated records

                // Define pager parameters
                $pageNumber = ($request->has('pageNumber')) ? $request->get('pageNumber') : config('pager.pageNumber');
                $recordsPerPage = ($request->has('recordsPerPage')) ? $request->get('recordsPerPage') : config('pager.recordsPerPage');

                $skip = ($pageNumber-1) * $recordsPerPage;

                $take = $recordsPerPage;

                // Get project list
                $projects = new Project;

                // To get list of projects assigned to Master Agents,Agents or Affiliates
                // if($user['role'] != 'Project Manager')
                //     $projects = $projects->select($this->table.'.*')->rightJoin('projectAgents','project.id','=','projectAgents.projectId')->where('projectAgents.userId',$user['id'])->where('project.deployedAt','!=',NULL);
                // else
                //     //To get list of projects created by Project Manager
                //     $projects = $projects->where('createdBy',$user['id']);

                if($user['role'] == 'Admin' || $user['role'] == 'User Manager')
                {
                    $projects = $projects->select('id', 'name');
                } else if($user['role'] == 'Project Manager') {
                    $projects = $projects->select($this->table.'.id', $this->table.'.name')->rightJoin('assignProjects','project.id','=','assignProjects.projectId')->where('assignProjects.userId',$user['id']);
                }

                if ($request->has('search')) {
                    // Decode json in to php array
                    $search = json_decode($request->get('search'),true);
                    // Get only required params
                    $search = array_filter($search, function($k){
                        return $k == 'id' || $k == 'name' || $k == 'address' || $k == 'client'  || $k == 'noOfUnits' || $k == 'totalUnits' || $k == 'commercialSpace' || $k == 'totalCommercialSpace' || $k == 'size' || $k == 'totalSize' || $k == 'developerCompany';
                    }, ARRAY_FILTER_USE_KEY);

                    foreach ($search as $field => $value) {
                       $projects = $projects->where($this->table.'.'.$field, 'like', '%'.$value.'%');
                    }
                }
                //total records
                $totalRecords = $projects->count();

                $projects = $projects->orderBy($this->table.'.'.$sortBy, $sortOrder)
                        ->skip($skip)
                        ->take($take);

                $projects = $projects->get();

                $filteredRecords = count($projects);

                $pager = ['sortBy'      => $sortBy,
                    'sortOrder'         => $sortOrder,
                    'pageNumber'        => $pageNumber,
                    'recordsPerPage'    => $recordsPerPage,
                    'totalRecords'      => $totalRecords,
                    'filteredRecords'   => $filteredRecords];
            }

            return createResponse(config('httpResponse.SUCCESS'),
                "projects list.",
                ['data' => $projects],
                $pager);

        } catch (\Exception $e) {
            \Log::error("project index ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Error while listing project",
                ['error' => 'Server error.']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function projectCreate(Request $request)
    {
        try
        {
            $validator = app('validator')->make($request->all(), [
                //normal project details
                'name'                 => 'required',
                'address'              => 'required',
                'postalCode'           => 'required',
                //'state'                => 'required',
                'country'              => 'required',
                'threeDUrl'              => 'required',

                'client'               => 'required',
                'noOfUnits'            => 'required|numeric|max:1000|integer',
                'commercialSpace'      => 'required|numeric|integer',

                //developer details
                'developerCompany'     => 'required',
                'developerEmail'       => 'email',

                //architect details
                'architectCompany'     => 'required',
                'architectEmail'       => 'email',
                'image'                => 'image|mimes:jpeg,png,jpg,JPG,JPEG,PNG|max:300',

                //additional details
                //'additionalDeveloperName'       => 'required',
                //'additionalArchitectName'       => 'required',
                'additionalDeveloperEmail'      => 'email',
                'additionalArchitectEmail'      => 'email',
                ], []);

            if ($validator->fails())
                return createResponse(config('httpResponse.UNPROCESSED'),  "Request parameter missing.",  ['error' => $validator->errors()->first()]);

            // Upload image
            $image = "";
            if($request->hasFile('image'))
            {
                $errors = [];
                $image = 'project'.time().str_random(10).'_'.$request->file('image')->getClientOriginalExtension();
                $imagePath = base_path()."/public/images/projects/".$image;

                // Move and resize uploaded file
                if(!$request->file('image')->move(base_path()."/public/images/projects/", $image))
                    $errors[] = ['Could not upload project image! You may try again.'];

                if(count($errors))
                    return createResponse(config('httpResponse.UNPROCESSED'),
                        "Project image could not be processed. Please try to upload valid image.",
                        ['error' => "Project image could not be processed. Please try to upload valid image."]);
            }
            $user = getLoggedInUser();
            $template_ids = explode(",",$request->get('template_ids'));
            // store project details
            $project = Project::create([
                'name'              => $request->get('name'),
                'address'           => $request->get('address'),
                'postalCode'           => $request->get('postalCode'),
                'state'           => $request->get('state'),
                'country'           => $request->get('country'),
                'threeDUrl'           => $request->get('threeDUrl'),

                'client'            => $request->get('client'),
                'noOfUnits'         => $request->get('noOfUnits'),
                'commercialSpace'   => $request->get('commercialSpace'),
                'projectDescription'   => $request->get('projectDescription'),
                'image'             => $image,

                'developerCompany'  => $request->get('developerCompany'),
                'developerIncharge' => $request->get('developerIncharge'),
                'developerMobile'    => $request->get('developerMobile'),
                'developerEmail'   => $request->get('developerEmail'),
                'developerAddress'  => $request->get('developerAddress'),
                'developerPostalCode'  => $request->get('developerPostalCode'),
                'developerState'  => $request->get('developerState'),
                'developerCountry'  => $request->get('developerCountry'),

                'architectCompany'  => $request->get('architectCompany'),
                'architectIncharge' => $request->get('architectIncharge'),
                'architectMobile'    => $request->get('architectMobile'),
                'architectEmail'   => $request->get('architectEmail'),
                'architectAddress'  => $request->get('architectAddress'),
                'architectPostalCode'  => $request->get('architectPostalCode'),
                'architectState'  => $request->get('architectState'),
                'architectCountry'  => $request->get('architectCountry'),

                'createdBy'         => $user['id']
            ]);

            // Create additional details
            $additional = [];
            $additional[] = [
                'projectId' => $project->id,

                'additionalDeveloperName'=> $request->get('additionalDeveloperName'),
                'additionalDeveloperAuthority'=> $request->get('additionalDeveloperAuthority'),
                'additionalDeveloperIncharge'=> $request->get('additionalDeveloperIncharge'),
                'additionalDeveloperMobile'=> $request->get('additionalDeveloperMobile'),
                'additionalDeveloperEmail'=> $request->get('additionalDeveloperEmail'),
                'additionalDeveloperAddress'=> $request->get('additionalDeveloperAddress'),
                'additionalDeveloperPostalCode'=> $request->get('additionalDeveloperPostalCode'),
                'additionalDeveloperState'=> $request->get('additionalDeveloperState'),
                'additionalDeveloperCountry'=> $request->get('additionalDeveloperCountry'),

                'additionalArchitectName'=> $request->get('additionalArchitectName'),
                'additionalArchitectAuthority'=> $request->get('additionalArchitectAuthority'),
                'additionalArchitectIncharge'=> $request->get('additionalArchitectIncharge'),
                'additionalArchitectMobile'=> $request->get('additionalArchitectMobile'),
                'additionalArchitectEmail'=> $request->get('additionalArchitectEmail'),
                'additionalArchitectAddress'=> $request->get('additionalArchitectAddress'),
                'additionalArchitectPostalCode'=> $request->get('additionalArchitectPostalCode'),
                'additionalArchitectState'=> $request->get('additionalArchitectState'),
                'additionalArchitectCountry'=> $request->get('additionalArchitectCountry'),

                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            AdditionalProjectDetail::insert($additional);

            // Create units based on units count
            $units = [];
            for ($i=0; $i < ($request->get('noOfUnits') + $request->get('commercialSpace')); $i++)
                $units[] = ['projectId' => $project->id, 'status'=>'available', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'settled' => 'no', 'publish' => 'yes'];

            \App\Unit::insert($units);

            //Create template for projects
            $template = '';
            if($template_ids) {
                foreach($template_ids as $template_id) {
                    $template = ['projectId' => $project->id, 'templateId'=>$template_id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
                    ProjectTemplate::insert($template);
                }

            }
            return createResponse(config('httpResponse.SUCCESS'),
                "The project has been added successfully.",
                ['message' => 'The project has been added successfully']);


        } catch (\Exception $e) {
            \Log::error("Project creation failed ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Something went wrong. Could not create project.",
                ['error' => 'Something went wrong. Could not create project.'.$e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function projectShow($id)
    {
        try
        {
            $user = getLoggedInUser();

            $project = Project::where('project.id', $id);

            switch ($user['role']) {
                case 'Admin':
                    $project->with(['units','agents','affiliates','masterAgents', 'marketingFiles', 'additionalDetail', 'projectTemplates']);
                    break;

                case 'User Manager':
                    $project->with(['units','agents','affiliates','masterAgents', 'marketingFiles', 'additionalDetail', 'projectTemplates']);
                    break;

                case 'Project Manager':
                    $project->with(['units','agents','affiliates','masterAgents', 'marketingFiles', 'additionalDetail', 'projectTemplates']);
                    break;

                case 'Master Agent' :
                    if(\App\ProjectAgent::where('projectId', $id)->where('userId', $user['id'])->count())
                        $project->with(['agents','affiliates'])
                            ->where('deployedAt', '!=', NULL)
                            ->with(['units'])->with('marketingFiles');
                    break;

                case 'Agent' :
                     $project->where('deployedAt', '!=', NULL)
                        ->with(['affiliates', 'marketingFiles'])
                        ->with(['units' => function($query) use ($user) {
                            $query->join('unitAgents', 'unitAgents.unitId', '=', 'unit.id')
                                ->where('unitAgents.userId', '=',$user['id'])
                                //->where('unit.publish','yes')
                                ->select('unit.*');
                        }]);
                    break;

                case 'Affiliate' :
                    $project->where('deployedAt', '!=', NULL)
                        ->with(['units' => function($query) use ($user) {
                            $query->join('unitAgents', 'unitAgents.unitId', '=', 'unit.id')
                                ->where('unitAgents.userId', '=',$user['id'])
                                //->where('unit.publish','yes')
                                ->select('unit.*');
                        }])->with('marketingFiles');
                    break;

                default:
                    # code...
                    break;
            }

            $project = $project->get();

            if(!count($project))
                return createResponse(config('httpResponse.SERVER_ERROR'),
                    "Project doesn't exist.",
                    ['error' => 'Project does not exist.']);

            //send project information
            return createResponse(config('httpResponse.SUCCESS'),
                "project details",
                ['data' => $project->first()]);

        } catch (\Exception $e) {
            \Log::error("project details api failed : ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Could not get project details",
                ['error' => 'Could not get project details.']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function projectUpdate(Request $request, $id)
    {
        try
        {
            $validator = app('validator')->make($request->all(),[
                //'size'                 => 'numeric|integer',
                //'totalSize'            => 'numeric|integer',
                'developerEmail'       => 'email',
                'architectEmail'       => 'email',
                'additionalDeveloperEmail'  => 'email',
                'additionalArchitectEmail'  => 'email',
                'masterAgents'         => 'json',
                'image'                => 'image|mimes:jpeg,png,jpg,JPG,JPEG,PNG|max:300',
                'removeImage'          => 'in:0,1',
                'deploy'               => 'in:0,1',
                'delete'               => 'in:0,1',
                'marketing'            => 'array',
                'marketing.*'          => 'mimes:jpg,png,pdf,jpeg,ppt,JPG,JPEG,PNG,PDF,PPT|max:50000',
                ],['marketing.array'     =>  'Marketing files must be an array.',
                'marketing.*.mimes'    =>  'Only pdf,ppt,jpg and png files are allowed.',
                'marketing.*.max'      =>  'The file size should not be more than 50 MB.']);

            // If validation fails then return error response
            if($validator->fails())
                return createResponse(config('httpResponse.UNPROCESSED'),
                "Request parameter missing.",
                ['error' => $validator->errors()->first()]);

            $user = \App\User::find(getLoggedinUser('id'));
            $template_ids = explode(",",$request->get('template_ids'));

            switch ($user['role']) {
                case 'Admin':
                    $project = Project::where('createdBy',$user->id)->find($id);
                break;
                case 'Project Manager':
                    $project = Project::select('project.id')->join('assignProjects','project.id','=','assignProjects.projectId')
                                ->where('project.id',$id)
                                ->where('assignProjects.userId',$user->id)->first();
                break;
            }

            if(!$project)
                return createResponse(config('httpResponse.SERVER_ERROR'),
                    "Project doesn't exist.",
                    ['error' => 'Project does not exist.']);

            $updateData = array();
            $updateData = filterFields([
                'name', 'address', 'postalCode', 'state', 'country', 'threeDUrl',
                'client', 'projectDescription',
                'developerCompany', 'developerIncharge', 'developerEmail','developerMobile','developerAddress', 'developerPostalCode', 'developerState', 'developerCountry',
                'architectCompany','architectIncharge', 'architectEmail','architectMobile','architectAddress','architectPostalCode', 'architectState', 'architectCountry'], $request);

            if($request->has('deploy') && $request->get('deploy') == 1)
            {
                event(new DeploymentNoticeEvent($project));
                $updateData['deployedAt'] = \Carbon\Carbon::now();
            }

            // Agents assigned to unit
            if($request->has('masterAgents'))
            {
                $masterAgents  = json_decode($request->get('masterAgents'),true);

                $validator = app('validator')->make(['masterAgents'=> $masterAgents], [
                        'masterAgents'   => 'array|max:1',
                        'masterAgents.*' => 'numeric|exists:user,id,role,Master Agent'
                    ], []);

                if ($validator->fails())
                    return createResponse(config('httpResponse.UNPROCESSED'),  "Request parameter missing.", ['error' => $validator->errors()->first()]);

                // To delete the extra master agent from table
                $agents = ProjectAgent::where(['projectId' => $id , 'role' => 'Master Agent'])->pluck('userId')->toArray();
                // Get removed data ids
                $removedData = array_diff($agents, $masterAgents);
                if(count($removedData))
                    ProjectAgent::whereIn('userId',$removedData)->where('projectId' , $id)->forceDelete();
                // Get newly added data
                $newData = array_diff($masterAgents, $agents);
                if(count($newData))
                {
                    $projectAgents = [];
                    foreach ($newData as $data)
                        $projectAgents[] = ['projectId' => $id, 'role' => 'Master Agent', 'userId' => $data, 'createdBy' => $user->id, 'created_at' => date('Y-m-d H:i:s')];

                    ProjectAgent::insert($projectAgents);
                }
            }

            // Image Upload
            $projectImage = "";
            if($request->hasFile('image'))
            {
                $existingImage = $project->image;
                $errors = [];
                $projectImage = 'project'.time().str_random(10).'_'.$request->file('image')->getClientOriginalExtension();
                $imagePath = base_path()."/public/images/projects/".$projectImage;

                // Move and resize uploaded file
                if(!$request->file('image')->move(base_path()."/public/images/projects/", $projectImage))
                    $errors[] = ['Could not upload project image! You may try again.'];

                if($existingImage != "" && file_exists(base_path()."/public/images/projects/".$existingImage))
                    unlink(base_path()."/public/images/projects/".$existingImage);

                if(count($errors))
                    return createResponse(config('httpResponse.UNPROCESSED'),
                        "Project image could not be processed. Please try to upload valid image.",
                        ['error' => "Project image could not be processed. Please try to upload valid image."]);

                $updateData['image'] = $projectImage;
            }

            // Marketing Image Upload
            if($request->hasFile('marketing'))
            {
                $marketings = $request->file('marketing');
                $this->uploadFiles($marketings, $id, 'marketing');
            }

            if($request->has('removeImage') && $request->get('removeImage'))
            {
                if($project->image != "" && file_exists(base_path()."/public/images/projects/".$project->image))
                    unlink(base_path()."/public/images/projects/".$project->image);

                $updateData['image'] = "";
            }

            $updateData['updatedBy'] = $user->id;
            $project->update($updateData);

            // Additional data update by project id
            $additional = AdditionalProjectDetail::where('projectId',$id)->first();
            if($additional) {

            $updateAdditionData = array();
            $updateAdditionData = filterFields([
                'additionalDeveloperName', 'additionalDeveloperAuthority', 'additionalDeveloperIncharge','additionalDeveloperMobile','additionalDeveloperEmail', 'additionalDeveloperAddress', 'additionalDeveloperPostalCode', 'additionalDeveloperState', 'additionalDeveloperCountry',
                'additionalArchitectName','additionalArchitectAuthority', 'additionalArchitectIncharge','additionalArchitectMobile','additionalArchitectEmail','additionalArchitectAddress', 'additionalArchitectPostalCode', 'additionalArchitectState', 'additionalArchitectCountry'], $request);


            $additional->update($updateAdditionData);
            }

            //Create template for projects
            $template = '';
            if($template_ids) {
                ProjectTemplate::where('projectId',$id)->delete();
                foreach($template_ids as $template_id) {
                    $template = ['projectId' => $project->id, 'templateId'=>$template_id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
                    ProjectTemplate::insert($template);
                }

            }

            //check if project updated successfully
            return createResponse(config('httpResponse.SUCCESS'),
                "The data has been updated successfully.",
                ['data' => $project, 'message' => 'The data has been updated successfully.']);

        } catch (\Exception $e) {
            \Log::error("project update ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Could not update project",
                ['error' => 'Server error.'.$e->getMessage()]);
        }
    }

    public function projectDestroy(Request $request, $id)
    {
        try
        {
            $user = getLoggedinUser();
            // Check if user has verified password or not
            // if(! verifyPasswordToken(array_merge($request->all(), ['userId' => $user['id']])))
            //     return createResponse(config('httpResponse.UNPROCESSED'),
            //         "Password verification failed. Please try again.",
            //         ['error' => 'Password verification failed. Please try again.']);

            app('db')->beginTransaction();

            // Get project details
            $project = Project::where('createdBy',$user['id'])->where('id', $id)->get();

            if(!count($project))
                return createResponse(config('httpResponse.SUCCESS'),
                    "Either selected project has already been deleted or you do not have privileges to delete the project.",
                    ['message' => 'Either selected project has already been deleted or you do not have privileges to delete the project.']);

            $project = $project->first();

            // Delete project image if exists
            if($project->image != "" && file_exists(base_path()."/public/images/projects/".$project->image))
                unlink(base_path()."/public/images/projects/".$project->image);

            // Delete project data
            $project->delete();
            // Detele unit, agent details
            Unit::where('projectId',$id)->delete();
            UnitAgent::where('projectId',$id)->delete();
            ProjectAgent::where('projectId',$id)->delete();
            AdditionalProjectDetail::where('projectId',$id)->delete();
            AssignProject::where('projectId',$id)->delete();

            $images = Images::where('projectId',$id)->get(['id','path','type']);
            foreach($images as $image)
            {
                if($image->path != ""  && file_exists(base_path()."/public/images/".$image->type."/".$image->path))
                    unlink(base_path()."/public/images/".$image->type."/".$image->path);
            }

            Images::where('projectId',$id)->delete();
            app('db')->commit();

            return createResponse(config('httpResponse.SUCCESS'),
                "The project has been deleted successfully.",
                ['message' => 'The project has been deleted successfully.']);

        } catch (\Exception $e) {
            app('db')->rollback();
            \Log::error("project deletion failed : ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Could not delete project",
                ['error' => 'Server error.']);
        }
    }

    /**
     * Project assigned to user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function projectAssign(Request $request) {
        try
        {
            $validator = app('validator')->make($request->all(), [
                'user_ids'                 => 'required',
                'project_ids'              => 'required',
                ], []);

            if ($validator->fails())
                return createResponse(config('httpResponse.UNPROCESSED'),  "Request parameter missing.",  ['error' => $validator->errors()->first()]);

            $project_ids = $request->get('project_ids');
            $user_ids = $request->get('user_ids');

            $assign = [];
            $is_assign = false;
            foreach ($project_ids as $key => $id) {
                foreach ($user_ids as $key => $uid) {
                    $getAssign = AssignProject::where('projectId', $id)->where('userId' , $uid)->first();
                    if($getAssign === null) {
                        $is_assign = true;
                        $assign[] = ['projectId' => $id, 'userId'=>$uid, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
                    }
                }
            }
            if( $is_assign) {
                AssignProject::insert($assign);
                return createResponse(config('httpResponse.SUCCESS'),
                "The project has been assign successfully.",
                ['message' => 'The project has been assign successfully']);
            } else {
                return createResponse(config('httpResponse.UNPROCESSED'),
                "The project has already been assigned.",
                ['error' => "The project has already been assigned."]);
            }


        } catch (\Exception $e) {
            \Log::error("Project assign failed ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Something went wrong. Could not assign project.",
                ['error' => 'Something went wrong. Could not assign project.']);
        }

    }

    public function staff(Request $request,$id)
    {
        try
        {
            //get loggedin user
            $userId = getLoggedInUser('id');

            //check validation
            $validator = app('validator')->make($request->all(),[
                'sortOrder'      => 'in:asc,desc',
                'pageNumber'     => 'numeric|min:1',
                'recordsPerPage' => 'numeric|min:0',
                'search'         => 'json',
                'isEnable'       => 'in:0,1'
            ],[]);

            if ($validator->fails()) {
                return createResponse(config('httpResponse.UNPROCESSED'),
                    "Request parameter missing.",
                    ['error' => $validator->errors()->first()]);
            }

             //sorting
            $sortBy = ($request->has('sortBy')) ? $request->get('sortBy') : config('pager.sortBy');
            $sortOrder = ($request->has('sortOrder')) ? $request->get('sortOrder') : config('pager.sortOrder');
            $pager = [];

             // Check if all records are requested
            if ($request->has('records') && $request->get('records') == 'all') {

                $users = new User;
                $users = $users->select('user.*');

                if ($request->has('search')) {
                    // Decode json in to php array
                    $search = json_decode($request->get('search'),true);
                    // Get only required params
                    $search = array_filter($search, function($k){
                        return $k == 'id' || $k == 'title' || $k == 'firstName' || $k == 'lastName' || $k == 'companyName'  || $k == 'office' || $k == 'address' || $k == 'isEnable' || $k == 'role';
                    }, ARRAY_FILTER_USE_KEY);

                    if(isset($search['role']))
                    {
                        switch ($search['role'][0]) {
                            case 'Project Manager':
                                $users = $users->join('project', 'user.id', '=','project.createdBy')->where('project.id',$id);
                                break;
                            case 'Master Agent':
                                $users = $users->join('projectAgents', 'projectAgents.userId', '=', 'user.id')
                                    ->where('projectAgents.projectId',$id)->where('projectAgents.role','Master Agent');
                                break;
                            case 'Agent':
                                $users = $users->join('projectAgents', 'projectAgents.userId', '=', 'user.id')
                                   ->where('projectAgents.projectId',$id)->where('projectAgents.role','Agent');
                                break;
                            case 'Affiliate':
                                $users = $users->join('projectAgents', 'projectAgents.userId', '=', 'user.id')
                                   ->where('projectAgents.projectId',$id)->where('projectAgents.role','Affiliate');
                                break;
                            default:
                                $users = $users;
                                break;
                        }
                        unset($search['role']);
                    }

                    foreach ($search as $field => $value)
                       $users = $users->where('user.'.$field, 'like', '%'.$value.'%');
                }
                $users = $users = $users->orderBy('user.'.$sortBy, $sortOrder)->distinct()->get();

            } else { // Else return paginated records

                // Define pager parameters
                $pageNumber = ($request->has('pageNumber')) ? $request->get('pageNumber') : config('pager.pageNumber');
                $recordsPerPage = ($request->has('recordsPerPage')) ? $request->get('recordsPerPage') : config('pager.recordsPerPage');

                $skip = ($pageNumber-1) * $recordsPerPage;

                $take = $recordsPerPage;

                // Get user list
                $users = new User;
                $users = $users->select('user.*');
                 //dd($request->has('search'));
                if ($request->has('search')) {
                    // Decode json in to php array
                    $search = json_decode($request->get('search'),true);
                    // Get only required params
                    $search = array_filter($search, function($k){
                        return $k == 'id' || $k == 'title' || $k == 'firstName' || $k == 'lastName' || $k == 'companyName'  || $k == 'office' || $k == 'address' || $k == 'isEnable' || $k == 'role' || $k == 'name';
                    }, ARRAY_FILTER_USE_KEY);

                    if(isset($search['role']))
                    {
                        switch ($search['role'][0]) {
                            case 'Project Manager':
                                $users = $users->join('project', 'user.id', '=','project.createdBy')->where('project.id',$id);
                                break;
                            case 'Master Agent':
                                $users = $users->join('projectAgents', 'projectAgents.userId', '=', 'user.id')
                                    ->where('projectAgents.projectId',$id)->where('projectAgents.role','Master Agent');
                                break;
                            case 'Agent':
                                $users = $users->join('projectAgents', 'projectAgents.userId', '=', 'user.id')
                                   ->where('projectAgents.projectId',$id)->where('projectAgents.role','Agent');
                                break;
                            case 'Affiliate':
                                $users = $users->join('projectAgents', 'projectAgents.userId', '=', 'user.id')
                                   ->where('projectAgents.projectId',$id)->where('projectAgents.role','Affiliate');
                                break;
                            default:
                                $users = $users;
                                break;
                        }
                        unset($search['role']);
                    }

                    // Search users by name
                    if(isset($search['name']))
                    {
                       $users = $users->where(function($query) use ($search) {
                            $query->where('user.'.'firstName', 'like', '%'.$search['name'].'%')
                                ->orWhere('user.'.'lastName', 'like', '%'.$search['name'].'%');
                        });
                        unset($search['name']);
                    }

                    foreach ($search as $field => $value) {
                       $users = $users->where('user.'.$field, 'like', '%'.$value.'%');
                    }
                }
                //total records
                $totalRecords = $users->count();

                $users = $users->orderBy('user.'.$sortBy, $sortOrder)
                        ->skip($skip)
                        ->take($take);

                $users = $users->distinct()->get();

                $filteredRecords = count($users);

                $pager = ['sortBy'      => $sortBy,
                    'sortOrder'         => $sortOrder,
                    'pageNumber'        => $pageNumber,
                    'recordsPerPage'    => $recordsPerPage,
                    'totalRecords'      => $totalRecords,
                    'filteredRecords'   => $filteredRecords];
            }

            return createResponse(config('httpResponse.SUCCESS'),
                "user list.",
                ['data' => $users],
                $pager);

        } catch (\Exception $e) {
            \Log::error("user index ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Error while listing user",
                ['error' => 'Server error.']);
        }
    }

    public function uploadFiles($files, $projectId, $type)
    {
        $unitImages = [];
        foreach ($files as $file)
        {
            $fileOriginalName = $file->getClientOriginalName();
            $fileName = $type.time().str_random(10).'.'.$file->guessExtension();

            Storage::disk('local')->put('projectFiles/'.$fileName, file_get_contents($file));

            $unitImages[] = ['unitId' => 0, "projectId" => $projectId, "name" => $fileOriginalName, "type" => $type, "path" => $fileName, 'created_at' => date('Y-m-d H:i:s')];
        }

        Images::insert($unitImages);
    }

    public function deleteFile($projectId, $id)
    {
        try
        {
            // GEt file details
            $file = Images::where('projectId',$projectId)->find($id);
            $user = getLoggedInUser();
            if(!$file)
                return createResponse(config('httpResponse.UNPROCESSED'),
                    "File doesn't exist.",
                    ['error' => 'File does not exist.']);

             // Check if the project belongs to logged in user
            if(! \App\Project::where('id', $projectId)->where('createdBy', $user['id'])->count())
                return createResponse(config('httpResponse.UNPROCESSED'),
                    "You do not have privilege to remove this file.",
                    ['error' => 'You do not have privilege to remove this file.']);

            $existingFile = $file->path;

            if(Storage::disk('local')->exists("projectFiles/".$existingFile))
                Storage::disk('local')->delete("projectFiles/".$existingFile);

            $file->delete();

            //check if project updated successfully
            return createResponse(config('httpResponse.SUCCESS'),
                "File has been deleted successfully.",
                ['message' => 'File has been deleted successfully.']);

        } catch (\Exception $e) {
            \Log::error("File deletion failed :  ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Could not delete file",
                ['error' => 'Server error.']);
        }
    }

     public function downloadFile(Request $request, $id, $fileId)
    {
        try
        {
            $user = getLoggedInUser();

            $project = Project::find($id);

            if(! $project)
                return createResponse(config('httpResponse.UNPROCESSED'), "The project does not exist.", ['error' => 'The project does not exist.']);

            switch ($user['role']) {
                case 'Project Manager':
                    if(! \App\Project::where('id', $project->id)->where('createdBy', $user['id'])->count())
                        return createResponse(config('httpResponse.UNPROCESSED'), "The unit does not belongs to you.", ['error' => 'The unit does not belongs to you.']);
                    break;

                case 'Master Agent' :
                    if(!\App\ProjectAgent::where('projectId', $project->id)->where('userId', $user['id'])->count())
                        return createResponse(config('httpResponse.UNPROCESSED'), "The unit does not belongs to you.", ['error' => 'The unit does not belongs to you.']);
                    break;

                case 'Agent' :
                case 'Affiliate' :
                    if(!\App\UnitAgent::where('projectId', $project->id)->where('userId', $user['id'])->count())
                        return createResponse(config('httpResponse.UNPROCESSED'), "The unit does not belongs to you.", ['error' => 'The unit does not belongs to you.']);
                    break;

                default:
                    return createResponse(config('httpResponse.SERVER_ERROR'), "Could not get unit details", ['error' => 'Could not get unit details.']);
            }

            $image = \App\Images::where('projectId', $id)->find($fileId);

            return response()->download(storage_path('app/projectFiles/'.$image->path));
        } catch (\Exception $e) {
            \Log::error("marketing file download api failed : ".$e->getMessage());
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "File download failed.",
                ['error' => 'File download failed.']);
        }
    }


    public function projectFilesUpload(Request $request)
    {
        try
        {
            app('db')->beginTransaction();
            $validator = app('validator')->make($request->all(), [
                'project_id' => 'required|exists:project,id',
                'image_files' => 'required'
            ]);

            if ($validator->fails()) {
                return createResponse(config('httpResponse.UNPROCESSED'),
                    "Request parameter missing.",
                    ['error' => $validator->errors()->first()]);
            }

            //get loggedin user
            $user = getLoggedInUser();
            $allowedfileExtension=['pdf','jpg','png'];
            $files = $request->file('image_files');
            $errors = [];
            $data = [];
            $projectImages = [];
            foreach ($files as $file)
            {
                $extension = $file->getClientOriginalExtension();
                $getSize = $file->getSize();
                $check = in_array($extension,$allowedfileExtension);

                if($check) {
                    $fileOriginalName = $file->getClientOriginalName();
                    $fileName = time().str_random(10).'_'.str_replace(" ", "", $fileOriginalName);
                    Storage::disk('local')->put('projectFiles/'.$fileName, file_get_contents($file));

                    $data[] = ProjectImage::create([
                            "projectId" => $request->get('project_id'),
                            "image" => $fileName,
                            'fileSize' => $this->size_as_kb($getSize),
                            'created_at' => date('Y-m-d H:i:s')
                    ]);
                } else {
                    return createResponse(config('httpResponse.UNPROCESSED'),
                    "Invalid image format.",
                    ['error' => "Invalid image format"]);
                }
            }

            //$data = ProjectImage::insert($projectImages);
            //$data = ProjectImage::create($projectImages);

            app('db')->commit();
            //check if Contact deployed successfully
            return createResponse(config('httpResponse.SUCCESS'),
            "The Upload Image has been added successfully.",
            ['message' => 'The Upload Image has been added successfully', 'data' => $data]);

        } catch (\Exception $e) {
            app('db')->rollback();
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Something went wrong. Could not Upload Image.",
                ['error' => 'Something went wrong. Could not Upload Image.'.$e->getMessage()]);
        }


    }

    public function size_as_kb($yoursize) {
        if($yoursize < 1024) {
          return "{$yoursize} bytes";
        } elseif($yoursize < 1048576) {
          $size_kb = round($yoursize/1024);
          return "{$size_kb} KB";
        } else {
          $size_mb = round($yoursize/1048576, 1);
          return "{$size_mb} MB";
        }
    }

    public function projectFilesDelete(Request $request)
    {
        try
        {
            app('db')->beginTransaction();
            $validator = app('validator')->make($request->all(), [
                'project_id' => 'required|exists:project,id',
                'file_ids' => 'required'
            ]);

            if ($validator->fails()) {
                return createResponse(config('httpResponse.UNPROCESSED'),
                    "Request parameter missing.",
                    ['error' => $validator->errors()->first()]);
            }

            $files = $request->get('file_ids');


            foreach ($files as $id)
            {
                $file = ProjectImage::find($id);

                if(!$file)
                    return createResponse(config('httpResponse.UNPROCESSED'),
                        "File doesn't exist.",
                        ['error' => 'File does not exist.']);

                    $existingFile = $file->image;

                    if(Storage::disk('local')->exists("projectFiles/".$existingFile))
                        Storage::disk('local')->delete("projectFiles/".$existingFile);

                    $file->delete();

                }
                app('db')->commit();
                return createResponse(config('httpResponse.SUCCESS'),
                "File has been deleted successfully.",
                ['message' => 'File has been deleted successfully.']);


        } catch (\Exception $e) {
            app('db')->rollback();
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Could not delete file",
                ['error' => 'Server error.'.$e->getMessage()]);
        }
    }

    public function projectFilesList(Request $request, $project_id)
    {
        try
        {
            //get loggedin user
            $user = getLoggedInUser();
            $ProjectImage = new ProjectImage;

            $ProjectImage = $ProjectImage->where('projectId',$project_id)->get();

            return createResponse(config('httpResponse.SUCCESS'),
                "ProjectImage list.",
                ['data' => $ProjectImage]
            );

        } catch (\Exception $e) {
            return createResponse(config('httpResponse.SERVER_ERROR'),
                "Error while listing ProjectImage",
                ['error' => 'Server error.'.$e->getMessage()]);
        }
    }
}